f(x1, x2) = -(np.sin(2*np.pi*x1)**3 * np.sin(2*np.pi*x2)) / (x1**3 * (x1 + x2))
g1(x1, x2) = x1**2 - x2 + 1 <= 0
g2(x1, x2) = 1 - x1 + (x2 - 4)**2 <= 0
